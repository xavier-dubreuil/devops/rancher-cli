FROM debian:latest

ARG VERSION

RUN echo 'debconf debconf/frontend select Noninteractive' | debconf-set-selections && \
    apt-get update && \
    apt-get upgrade -y
RUN apt-get install --no-install-recommends -y nano curl ca-certificates
RUN cd /tmp && \
    curl -k -o rancher-cli.tar.gz "https://releases.rancher.com/cli/v${VERSION}/rancher-linux-amd64-v${VERSION}.tar.gz" && \
    tar xzf rancher-cli.tar.gz && \
    cp rancher-v${VERSION}/rancher /usr/local/bin/ && \
    chmod +x /usr/local/bin/rancher && \
    rm -rf rancher* && \
    cd -
RUN apt-get purge -y --auto-remove && \
    rm -rf /var/lib/apt/lists/*

WORKDIR /usr/src
